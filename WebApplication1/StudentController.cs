﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication1
{
    [Route("api/v1/student")]
    public class StudentController : Controller
    {
        [HttpPost]
        public void CreateProduct([FromBody]StudentModel product) { }
        [HttpGet(Name ="GetStudent")]
        public ActionResult<List<StudentModel>> Get([FromQuery] string Q)
        {
            var students = new List<StudentModel> {
                new StudentModel{
                    Name = "Jack",
                    BirthDay = new DateTimeOffset(1995,1,1,0,0,0,new TimeSpan(7,0,0)),
                    Hobby = "Gaming"
                },
                new StudentModel{
                    Name = "Budi",
                    BirthDay = new DateTimeOffset(2000,10,1,0,0,0,new TimeSpan(7,0,0)),
                    Hobby = "Reading"
                },
                new StudentModel{
                    Name = "Andy",
                    BirthDay = new DateTimeOffset(1999,5,1,0,0,0,new TimeSpan(7,0,0)),
                    Hobby = "Writing"
                }
            };
            return Ok(students);
        }
    }
}
